# Una collezione di files `.gitignore`

Questo è un repository che raccoglie i files [` .gitignore` ].
Potete usare uno dei modelli disponibili,  per popolare il vostro ` .gitignore`
nel vostro progetto.


## Struttura repository

Ogni modello di `.gitignore` è nominato in base alla tipologia di linguaggio,
editor, o tools, che genera file con estensioni, che non dovrebbero essere
committate sui vostri repositori.

## Contributi

Se volete aiutarmi a portare avanti il progetto e fornire dei contributi,
dovete seguire queste linee guida.

- **Fornite sempre un descrizione dell'applicazione o un link alla pagina del progetto **.
Se non è popolare, c'è il rischio che molti nonne conoscano l'esistenza,
la tipologia di utilizzo, il framework o il linguaggio usato.

- **Fornite un link alla documentazione**.
  Se non è disponibile, il meglio che potete fare, e spiegare perché, quella
  tipologia di files viene ignorata.

- **Spiegare sempre i cambiamenti**. Anche se vi sembra stupido,
fornite sempre il motivo del perché avete effettuto una modifica.
Tenete conto che questo progetto è rivolto a tutti, e quindi è utile che
gli altri, comprendano il perché della modifica.

- **Specificate sempre, la tipologia della modifica**. Se apportate una modifica
ad un linguaggio, ad un framework o ad un tool, specificate sempre di cosa si
tratta, così da non arrecare confusione nella struttura del progetto.

In generale, più dettagiliata sarà la vostra modifica, più in fretta verrà accettato
il vostro contributo

## Come contribuire

Ecco come si dovrebbe proporre una modifica a questo progetto:

1. [Forka il progetto][fork] sul tuo account.
2. [Crea un branch][branch] per i cambiamenti che intendi fare.
3. Effettua le modifiche sul tuo fork.
4. [Invia una richiesta di pull ][pr] dal tuo branch di fork al branch `master` del progetto.

Utilizzando l'interfaccia web dedicata alle modifiche, potrete effettuare automaticamente
un fork del progetto, ed efettuare una richiesta di pull.

[fork]: https://confluence.atlassian.com/display/BITBUCKET/Forking+a+Repository
[branch]: https://confluence.atlassian.com/display/BITBUCKET/Branching+a+Repository
[pr]: https://confluence.atlassian.com/display/BITBUCKET/Work+with+pull+requests

## License

[MIT](./LICENSE).
[TEST]
